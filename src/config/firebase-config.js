import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getFirestore } from 'firebase/firestore';
import { getStorage } from 'firebase/storage';

const firebaseConfig = {
  apiKey: 'AIzaSyDyQmm5AByEnHjs3Jkc7f7n1RjPWgM1XiY',
  authDomain: 'codersmonkeys-6ae21.firebaseapp.com',
  projectId: 'codersmonkeys-6ae21',
  storageBucket: 'codersmonkeys-6ae21.appspot.com',
  messagingSenderId: '8486001515',
  appId: '1:8486001515:web:2ba8ebb1b1ee63c4370a2e',
  measurementId: 'G-CME7GC8K8S',
};

export const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const db = getFirestore(app);
export const storage = getStorage(app);
