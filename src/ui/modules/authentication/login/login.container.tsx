import { LoginFormFieldType, RegisterFormFieldType } from "@/types/forms";
import { useEffect,  } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { LoginView } from "./login.view";


import { getAuth, onAuthStateChanged } from "firebase/auth";
import { auth } from "@/config/firebase-config"
import { useToggle } from "@/hooks/use-toggle";
import { firebaseSignInUser } from "@/api/authentication";
import { toast } from "react-toastify";
import { useRouter } from "next/router";

export const LoginContainer = () => {
    const router = useRouter()
    const { value: isLoading, setValue: setIsLoading } = useToggle();

 
    const {
        handleSubmit,
        formState: { errors },
        register,
        setError,
        reset,
    } = useForm<LoginFormFieldType>();
    const handleSignInUser= async ({ email, password}: LoginFormFieldType) => {
            const {error} = await firebaseSignInUser(email, password);
            if (error) {
                setIsLoading(false);
                toast.error(error.message)
                return;
            }

            toast.success("Bienvenue sur Coders Monkeys");
            setIsLoading(false);
            reset();
            router.push("/mon-espace");
    }
    const onSubmit: SubmitHandler<LoginFormFieldType> = async (formData) => {
        setIsLoading(true);
        const { password } =formData;
        if (password.length <= 5) {
            setError("password",{
                type: "manual",
                message: "Ton mot de passe doit comporter 6 caractéres au minimum."
            })
            setIsLoading(false)
            return;
        }
        handleSignInUser(formData);
    }
    
    return (
    
    <LoginView 
        form={{
            errors,
            register,
            handleSubmit,
            onSubmit,
            isLoading
        }}
    />
    );
};