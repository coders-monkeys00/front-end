import { FormsType } from "@/types/forms";
import { Button } from "@/ui/design-system/button/button";
import { Input } from "@/ui/design-system/forms/input";
import { useState } from "react";

interface Props {
    form: FormsType;
}


export const RegisterForm = ({ form }: Props) => {
    const {
        isLoading,
        onSubmit,
        errors,
        register,
        handleSubmit
    } = form;


    return (
       
        <form onSubmit={handleSubmit(onSubmit)} className="pt-8 pb-5 space-y-5">
    <Input
    isLoading={isLoading}
    placeholder="John@example.fr"
    type="email"
    register={register}
    errors={errors}
    errorMsg="Tu dois renseigner ce champ"
    id="email"
/>
<Input
    isLoading={isLoading}
    placeholder="Mot de passe"
    type="password"
    register={register}
    errors={errors}
    errorMsg="Tu dois renseigner ce champ"
    id="password"
/>
<Input
    isLoading={isLoading}
    placeholder="Comment nous as-tu connus ?"
    register={register}
    errors={errors}
    errorMsg="Tu dois renseigner ce champ"
    id="how_did_hear"
/>
            <Button isLoading={isLoading} type="submit" fullWidth>{"S'inscrire"}</Button>
    </form>
      
    )
}