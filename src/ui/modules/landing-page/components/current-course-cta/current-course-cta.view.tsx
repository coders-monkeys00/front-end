import { Container } from "@/ui/components/container/container";
import { Typography } from "@/ui/design-system/typography/typography";
import Image from "next/image"
import { RiPlayCircleLine } from "react-icons/ri";

export const CurrentCourseCtaView = () => {
    return (
    
    
    <div className="bg-gray-300">
        <Container className="py-24 text-center">
<Typography variant="h2" component="h2" className="mb-2.5">
    Formations React.js gratuite</Typography>
    <Typography variant="lead" component="h3" className="mb-5">
    Apprends à coder l’app des singes codeurs
    </Typography>
    <Typography variant="caption3" component="p" theme="gray" className="mb-16">
    Si tu veux un CV plus sexy que ton ex, suis cette formation complète !
    </Typography>
    <a href="/#" target="_blank">
        <div className="relative bg-gray-400 rounded h-[626px]">
            <div className="relative h-full bg-gray z-10 rounded opacity-0 animate hover:opacity-95 flex flex-col gap-2 justify-center items-center text-white">
                <RiPlayCircleLine size={42}/>
                <Typography variant="caption2" theme="white" className="uppercase" weight="medium">
                    Lire la formation
                </Typography>
            </div>
            <Image alt="image current course" fill src="assets/images/coursecurrent.png"/>
        </div>
        </a>    
        </Container>
        

    </div>


    );
};