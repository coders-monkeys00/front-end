import { useAuth } from "@/context/AuthUserContext";
import { useToggle } from "@/hooks/use-toggle";
import { BaseComponentProps } from "@/types/onboarding-steps-list";
import { OnboardingFooter } from "../../footer/onboarding-footer";
import { Typography } from "@/ui/design-system/typography/typography";
import { OnboardingTabs } from "../../tabs/onboarding-tabs";
import { Container } from "@/ui/components/container/container";
import { Logo } from "@/ui/design-system/logo/logo";
import { useCallback, useRef, useEffect, useState } from "react";
import ReactCanvasConfetti from "react-canvas-confetti";
import Image from "next/image";
import { firestoreUpdateDocument } from "@/api/firestore";
import { toast } from "react-toastify";

export const FinalStep = ({ isFinalStep }: BaseComponentProps) => {
    const { authUser, realodAuthUserData } = useAuth();
    const { value: isLoading, toggle } = useToggle();

    const refAnimationInstance = useRef<((opts: any) => void) | null>(null)
    const getInstance = useCallback((instance: any) => {
        refAnimationInstance.current = instance
    }, []);

    const makeShot = useCallback((particleRatio: number, opts: any) => {
        if (refAnimationInstance.current !== null) {
            refAnimationInstance.current({
                ...opts,
                origin: { y: 0.7 },
                particleCount: Math.floor(200 * particleRatio)
            })
        }
    }, [])

    const onFire = useCallback(() => {
        makeShot(0.25, {
            spread: 26,
            startVelocity: 55,

            })
    }, [])

   const [showGif, setShowGif] = useState(false); // Étape 1 : Déclaration de l'état pour afficher le GIF
    const [gifCount, setGifCount] = useState(0); // Étape 1 : Déclaration de l'état pour compter les affichages

    const handleCloseOnboarding = async () => {
        toggle();
        const { error } = await firestoreUpdateDocument("users", authUser.uid, {
            onboardingIsCompleted: true
        })

        if (error) {
            toggle();
            toast.error(error.message)
            return;
        }
        realodAuthUserData();
        toggle();

        // Augmente le compteur et affiche le GIF
        if (gifCount < 5) {
            setGifCount(prevCount => prevCount + 1);
            setShowGif(true);
        }
    };

    useEffect(() => {
        if (showGif) {
            // Crée un délai pour cacher le GIF après une certaine durée
            const timer = setTimeout(() => {
                setShowGif(false);
            }, 8300); // Délai de 2000 ms (2 secondes)

            // Nettoie le timeout si le composant est démonté avant que le délai expire
            return () => clearTimeout(timer);
        }
    }, [showGif]);

    return(
    
    <>
                <Image
                    src="/assets/svg/anim.gif"
                    alt="gif animated"
                    className="absolute z-40 top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2"
                    width={400}
                    height={400}
                />
                
            
            <div className="relative h-screen pb-[91px]">
                <div className="h-full overflow-auto">
                    <Container className="h-full ">
                        <div className="relative z-10 flex items-center h-full p-10">
                            <div className="w-full max-w-xl mx-auto space-y-5 pb-4.5">
                                <div className="flex justify-center">
                                    <Logo size="large" />
                                </div>
                                <Typography variant="h1" component="h1" className="text-center">
                                    Félicitation
                                </Typography>
                                <Typography variant="body-base" component="p" theme="gray" className="text-center">
                                    {"Tu fais maintenant partie de la tribu des singes codeurs ! Nous sommes ravis de t'accueillir parmi nous. Tu vas pouvoir te lancer dans l'aventure, partager tes projets les plus fous et rencontrer des développeurs aussi passionnés que toi. Alors prépare-toi, car notre communauté est prête à te secouer les branches et à te faire grimper au sommet de l'arbre du développement web !"}
                                </Typography>
                            </div>
                        </div>
                    </Container>
                </div>
                <OnboardingFooter
                    next={handleCloseOnboarding}
                    isFinalStep={isFinalStep}
                    isLoading={isLoading}
                />
            </div>
        </>
    );
};