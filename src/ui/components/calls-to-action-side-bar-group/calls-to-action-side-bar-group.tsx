import { Button } from "@/ui/design-system/button/button"
import { Typography } from "@/ui/design-system/typography/typography"
import Image from "next/image"

export const CallsToActionSideBarGroup = () => {
    return (
        <div className="relative flex flex-col justify-center gap-5 px-8 py-12 overflow-hidden rounded pb-44 bg-[#EAF8F4]">
            <Typography
                variant="lead"
                theme="black"
                weight="medium"
                className="text-center"
            > Rejoins-nous sur le groupe
            </Typography>
            <div className="flex justify-center">
                <Button
                    variant="success"
                    baseUrl="https://fr.linkedin.com/in/s%C3%A9bastien-lourdel-297715151?original_referer=https%3A%2F%2Fwww.google.com%2F"
                linkType="external">
                    Rejoindre
                </Button>
            </div>
            <Image width={183} height={183} src="/assets/svg/slacked.png" alt="pip donnation" className="absolute -bottom-10 transform -translate-x-1/2 left-1/2"/>
        </div>
        
    )
}