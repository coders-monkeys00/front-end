import { useAuth } from "@/context/AuthUserContext";
import { GUEST, REGISTERED } from "@/lib/session-status";
import { ScreenSpinner } from "@/ui/design-system/spinner/screen-spinner";
import { useRouter } from "next/router";

interface Props {
    children: React.ReactNode;
    sessionStatus?: string;
}

export const Session = ({ children, sessionStatus }: Props) => {
    const router = useRouter();
    const { authUserIsLoading, authUser } = useAuth();

    const onboardingIsCompleted = authUser?.userDocument?.onboardingIsCompleted;

    const shouldRedirectToOnboarding = () => {
        return (
            authUser &&
            !authUserIsLoading &&
            !onboardingIsCompleted &&
            router.asPath !== "/onboarding"
        );
    };

    const shouldRedirectToMonEspace = () => {
        return (
            authUser &&
            !authUserIsLoading &&
            onboardingIsCompleted &&
            router.asPath === "/onboarding"
        );
    };

    if (authUserIsLoading) {
        return <ScreenSpinner />;
    }

    if (shouldRedirectToOnboarding()) {
        router.push("/onboarding");
        return <ScreenSpinner />;
    }

    if (shouldRedirectToMonEspace()) {
        router.push("/mon-espace");
        return <ScreenSpinner />;
    }

    if (sessionStatus === GUEST) {
        if (!authUser) {
            return <>{children}</>;
        } else {
            router.push("/mon-espace");
            return <ScreenSpinner />;
        }
    }

    if (sessionStatus === REGISTERED) {
        if (authUser) {
            return <>{children}</>;
        } else {
            router.push("/connexion");
            return <ScreenSpinner />;
        }
    }

    if (!sessionStatus) {
        return <>{children}</>;
    }

    return <ScreenSpinner />;
};