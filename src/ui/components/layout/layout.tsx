import { SessionStatusTypes } from "@/types/SessionStatusTypes";
import { Breadcrumbs } from "../breadcrumbs/breadcrumbs"
import { Container } from "../container/container";
import { Footer } from "../navigation/footer"
import { Navigation } from "../navigation/navigation"
import { UserAccountNavigation } from "../navigation/user-account-navigation";
import { Session } from "../session/session";
import { CallsToActionSideBarContribution } from "../calls-to-action-side-bar-contribution/calls-to-action-side-bar-contribution";
import { CallsToActionSideBarGroup } from "../calls-to-action-side-bar-group/calls-to-action-side-bar-group";

interface Props {
    children: React.ReactNode;
    isdisplayBreadcrumbs?: boolean;
    withSidebar?: boolean;
    sessionStatus?: SessionStatusTypes;
}

export const Layout = ({ children, isdisplayBreadcrumbs = true, withSidebar, sessionStatus }: Props ) => {

    let view: React.ReactElement = <></>;

    if (withSidebar) {
        view = (
            <Container className="mb-14">
            <div className="grid grid-cols-12 gap-7">
                <div className="col-span-3 space-y-8">
                        <UserAccountNavigation />
                        <CallsToActionSideBarContribution />
                        <CallsToActionSideBarGroup />

                </div>
                <div className="col-span-9">{children}</div>

            </div>
            </Container>
        )
    }
    else {
        view = <>{children}</>
    }
    return (
        <Session sessionStatus={sessionStatus}>
        <Navigation />
        {isdisplayBreadcrumbs && <Breadcrumbs />}
        {view}
        <Footer />
        </Session>
    )
}