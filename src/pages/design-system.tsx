// COMPONENT
import { Seo } from '@/ui/components/seo/seo'
import { Navigation } from '@/ui/components/navigation/navigation';
import { Container } from '@/ui/components/container/container'

// DESIGN SYSTEM
import { Button } from '@/ui/design-system/button/button';
import { Typography } from '@/ui/design-system/typography/typography';
import { Spinner } from '@/ui/design-system/spinner/spinner';
import { Logo } from '@/ui/design-system/logo/logo'
import { Avatar } from '@/ui/design-system/avatar/avatar'

// ICO
import { LuBellRing } from "react-icons/lu";
import { FaReact } from "react-icons/fa";
import { MdOutlineSecurity } from "react-icons/md";

export default function DesignSystem() {
    return(
        <>
        <Seo title="Design system" description="design system"/>

        <Navigation />

        <Container className='py-10 space-y-5'>
        {/* TYPOGRAPHY */}
        <div className='space-y-2 p-8'>
          <Typography variant='caption2' weight='medium'>
            Typography
          </Typography>
          <div className='flex flex-col gap-2 p-5 border border-gray-400 divide-y-2 divide-gray-400 rounded'>
            <div className='pb-5 space-y-2'>
              <Typography variant='caption3' weight='medium'>
                Display
              </Typography>
              <Typography variant='display'>
                Nothing is impossible
              </Typography>
            </div>
  
            <div className='py-5 space-y-2'>
              <Typography variant="caption3" weight="medium">
                H1
              </Typography>
              <Typography variant='h1'>
                Nothing is impossible, the word itself says, im possible!
              </Typography>
            </div>
  
            <div className='py-5 space-y-2'>
              <Typography variant="caption3" weight="medium">
                H2
              </Typography>
              <Typography variant='h2'>
                Nothing is impossible, the word itself says, im possible!
              </Typography>
            </div>
  
            <div className='py-5 space-y-2'>
              <Typography variant="caption3" weight="medium">
                H3
              </Typography>
              <Typography variant='h3'>
                Nothing is impossible, the word itself says, im possible!
              </Typography>
            </div>
  
            <div className='py-5 space-y-2'>
              <Typography variant="caption3" weight="medium">
                H4
              </Typography>
              <Typography variant='h4'>
                Nothing is impossible, the word itself says, im possible!
              </Typography>
            </div>
  
            <div className='py-5 space-y-2'>
              <Typography variant="caption3" weight="medium">
                H5
              </Typography>
              <Typography variant='h5'>
                Nothing is impossible, the word itself says, im possible!
              </Typography>
            </div>
  
            <div className='py-5 space-y-2'>
              <Typography variant="caption3" weight="medium">
                LEAD
              </Typography>
              <Typography variant='lead'>
                Nothing is impossible, the word itself says, im possible!
              </Typography>
            </div>
  
            <div className='py-5 space-y-2'>
              <Typography variant="caption3" weight="medium">
                BODY-LG
              </Typography>
              <Typography variant='body-lg'>
                Nothing is impossible, the word itself says, im possible!
              </Typography>
            </div>
  
            <div className='py-5 space-y-2'>
              <Typography variant="caption3" weight="medium">
                BODY-BASE
              </Typography>
              <Typography variant='body-base'>
                Nothing is impossible, the word itself says, im possible!
              </Typography>
            </div>
  
            <div className='py-5 space-y-2'>
              <Typography variant="caption3" weight="medium">
                BODY-SM
              </Typography>
              <Typography variant='body-sm'>
                Nothing is impossible, the word itself says, im possible!
              </Typography>
            </div>
  
            <div className='py-5 space-y-2'>
              <Typography variant="caption3" weight="medium">
                CAPTION1
              </Typography>
              <Typography variant='caption1'>
                Nothing is impossible, the word itself says, im possible!
              </Typography>
            </div>
  
            <div className='py-5 space-y-2'>
              <Typography variant="caption3" weight="medium">
                CAPTION2
              </Typography>
              <Typography variant='caption2'>
                Nothing is impossible, the word itself says, im possible!
              </Typography>
            </div>
  
            <div className='py-5 space-y-2'>
              <Typography variant="caption3" weight="medium">
                CAPTION3
              </Typography>
              <Typography variant='caption3'>
                Nothing is impossible, the word itself says, im possible!
              </Typography>
            </div>
  
            <div className='py-5 space-y-2'>
              <Typography variant="caption3" weight="medium">
                CAPTION1
              </Typography>
              <Typography variant='caption1'>
                Nothing is impossible, the word itself says, im possible!
              </Typography>
            </div>
  
  
          </div>
  
        </div>
  
        <div className='flex items-start gap-7 p-8'>
            {/* SPINNERS */}
            <div className='space-y-2'>
                <Typography variant="caption2" weight="medium">
                  Spinners
                </Typography>
                <div className='flex items-center gap-2 p-5 border border-gray-400 rounded'>
                  <Spinner size='small'/>
                  <Spinner />
                  <Spinner size='large' />
                </div>
            </div>
  
             {/* AVATAR */}
             <div className='space-y-2'>
                <Typography variant="caption2" weight="medium">
                  Avatar
                </Typography>
                <div className='flex items-center gap-2 p-5 border border-gray-400 rounded'>
                  <Avatar src='/assets/images/avatar-test.png' alt='test' size='small' />
                  <Avatar src='/assets/images/avatar-test.png' alt='test'  />
                  <Avatar src='/assets/images/abstract.jpg' alt='test'  size='large' />
  
                </div>
            </div>
  
             {/* LOGO */}
             <div className='space-y-2'>
                <Typography variant="caption2" weight="medium">
                  Logo
                </Typography>
                <div className='flex items-center gap-2 p-5 border border-gray-400 rounded'>
                  <Logo size="very-small" />
                  <Logo size="small" />
                  <Logo />
                  <Logo size="large" />
  
                </div>
            </div>
  
            
        </div>
  
        {/* BUTTONS */}
        <div className='space-y-2 p-8'>
            <Typography variant='caption2' weight='medium'>
              Buttons
            </Typography>
            <div className='p-5 space-y-8 border border-gray-400 rounded p-8'>
            <div className='space-y-2'>
                  <Typography variant='caption3' weight='medium'>
                      Small
                  </Typography>
                  <div className='space-y-4 flex flex-wrap'>
                    <div className='flex items-center gap-2'>
                      <Button 
                        iconPosition='left' 
                        icon={{ icon: LuBellRing }} 
                        size="small"
                      >
                        Primary
                      </Button>
                      <Button
                          size='small'
                          icon={{ icon: LuBellRing }}
                          iconPosition="right"
                          variant='outline'
                      >
                        Accent
                      </Button>
                      <Button
                          variant='secondary'
                          size='small'
                      >
                        Secondary
                      </Button>
                      <Button
                          variant='disabled'
                          size='small'
                      >
                        Disabled
                      </Button>
                      <Button
                          variant='ico'
                          size='small'
                          icon={{ icon: MdOutlineSecurity }}
                      >
                        secondary
                      </Button>
                      <Button
                          variant='ico'
                          iconTheme='secondary'
                          size='small'
                          icon={{ icon: FaReact }}
                      >
                        secondary
                      </Button>
                      <Button
                          variant='ico'
                          iconTheme='gray'
                          size='small'
                          icon={{ icon: LuBellRing }}
                      >
                        gray
                      </Button>
                     
                      
                      
                    </div>
  
                  </div>
                  <div className='space-y-4 flex flex-wrap'>
                    <div className='flex items-center gap-2'>
                      <Button 
                        isLoading
                        iconPosition='left' 
                        icon={{ icon: LuBellRing }} 
                        size="small"
                      >
                        Primary
                      </Button>
                      <Button
                          isLoading
                          size='small'
                          icon={{ icon: LuBellRing }}
                          iconPosition="right"
                          variant='outline'
                      >
                        Accent
                      </Button>
                      <Button
                          variant='secondary'
                          size='small'
                          isLoading
                      >
                        Secondary
                      </Button>
                      <Button
                          variant='disabled'
                          size='small'
                          isLoading
                      >
                        Disabled
                      </Button>
                      <Button
                          variant='ico'
                          isLoading
                          size='small'
                      >
                        secondary
                      </Button>
                      <Button
                          variant='ico'
                          iconTheme='secondary'
                          isLoading
                          size='small'
                      >
                        secondary
                      </Button>
                      <Button
                          variant='ico'
                          iconTheme='gray'
                          isLoading
                          size='small'
                      >
                        gray
                      </Button>
                      
                      
                    </div>
  
                  </div>
                </div>
                <div className='space-y-2'>
                  <Typography variant='caption3' weight='medium'>
                      Medium
                  </Typography>
                  <div className='space-y-4 flex flex-wrap'>
                    <div className='flex items-center gap-2'>
                      <Button 
                        iconPosition='left' 
                        icon={{ icon: LuBellRing }} 
                        size="medium"
                      >
                        Primary
                      </Button>
                      <Button
                          size='medium'
                          icon={{ icon: LuBellRing }}
                          iconPosition="right"
                          variant='outline'
                      >
                        Accent
                      </Button>
                      <Button
                          variant='secondary'
                          size='medium'
                      >
                        Secondary
                      </Button>
                      <Button
                          variant='disabled'
                          size='medium'
                      >
                        Disabled
                      </Button>
                      <Button
                          variant='ico'
                          size='medium'
                          icon={{ icon: MdOutlineSecurity }}
                      >
                        secondary
                      </Button>
                      <Button
                          variant='ico'
                          iconTheme='secondary'
                          size='medium'
                          icon={{ icon: FaReact }}
                      >
                        secondary
                      </Button>
                      <Button
                          variant='ico'
                          iconTheme='gray'
                          size='medium'
                          icon={{ icon: LuBellRing }}
                      >
                        gray
                      </Button>
                     
                      
                      
                    </div>
  
                  </div>
                  <div className='space-y-4 flex flex-wrap'>
                    <div className='flex items-center gap-2'>
                      <Button 
                        isLoading
                        iconPosition='left' 
                        icon={{ icon: LuBellRing }} 
                        size="medium"
                      >
                        Primary
                      </Button>
                      <Button
                          isLoading
                          size='medium'
                          icon={{ icon: LuBellRing }}
                          iconPosition="right"
                          variant='outline'
                      >
                        Accent
                      </Button>
                      <Button
                          variant='secondary'
                          size='medium'
                          isLoading
                      >
                        Secondary
                      </Button>
                      <Button
                          variant='disabled'
                          size='medium'
                          isLoading
                      >
                        Disabled
                      </Button>
                      <Button
                          variant='ico'
                          isLoading
                          size='medium'
                      >
                        secondary
                      </Button>
                      <Button
                          variant='ico'
                          iconTheme='secondary'
                          isLoading
                          size='medium'
                      >
                        secondary
                      </Button>
                      <Button
                          variant='ico'
                          iconTheme='gray'
                          isLoading
                          size='medium'
                      >
                        gray
                      </Button>
                      
                      
                    </div>
  
                  </div>
                </div>
                <div className='space-y-2'>
                  <Typography variant='caption3' weight='medium'>
                      Large
                  </Typography>
                  <div className='space-y-4 flex flex-wrap'>
                    <div className='flex items-center gap-2'>
                      <Button 
                        iconPosition='left' 
                        icon={{ icon: LuBellRing }} 
                        size="large"
                      >
                        Primary
                      </Button>
                      <Button
                          size='large'
                          icon={{ icon: LuBellRing }}
                          iconPosition="right"
                          variant='outline'
                      >
                        Accent
                      </Button>
                      <Button
                          variant='secondary'
                          size='large'
                      >
                        Secondary
                      </Button>
                      <Button
                          variant='disabled'
                          size='large'
                      >
                        Disabled
                      </Button>
                      <Button
                          variant='ico'
                          size='large'
                          icon={{ icon: MdOutlineSecurity }}
                      >
                        secondary
                      </Button>
                      <Button
                          variant='ico'
                          iconTheme='secondary'
                          size='large'
                          icon={{ icon: FaReact }}
                      >
                        secondary
                      </Button>
                      <Button
                          variant='ico'
                          iconTheme='gray'
                          size='large'
                          icon={{ icon: LuBellRing }}
                      >
                        gray
                      </Button>
                     
                      
                      
                    </div>
  
                  </div>
                  <div className='space-y-4 flex flex-wrap'>
                    <div className='flex items-center gap-2'>
                      <Button 
                        isLoading
                        iconPosition='left' 
                        icon={{ icon: LuBellRing }} 
                        size="large"
                      >
                        Primary
                      </Button>
                      <Button
                          isLoading
                          size='large'
                          icon={{ icon: LuBellRing }}
                          iconPosition="right"
                          variant='outline'
                      >
                        Accent
                      </Button>
                      <Button
                          variant='secondary'
                          size='large'
                          isLoading
                      >
                        Secondary
                      </Button>
                      <Button
                          variant='disabled'
                          size='large'
                          isLoading
                      >
                        Disabled
                      </Button>
                      <Button
                          variant='ico'
                          isLoading
                          size='large'
                      >
                        secondary
                      </Button>
                      <Button
                          variant='ico'
                          iconTheme='secondary'
                          isLoading
                          size='large'
                      >
                        secondary
                      </Button>
                      <Button
                          variant='ico'
                          iconTheme='gray'
                          isLoading
                          size='large'
                      >
                        gray
                      </Button>
                      
                      
                    </div>
  
                  </div>
                </div>
            </div>
        </div>
  
  
        </Container>
  
        </>
    )
}